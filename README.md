# Buzzword bingo in Elm

```shell
# pane 1 ->
$ cd server && npm i                                     # install server deps
$ node server.js                                         # run api

# pane 2 ->
$ npm i -g elm@0.18.0 elm-live                           # install elm 0.18.0 and elm-live
$ elm-package install                                    # install deps
$ elm-live Bingo.elm --output=elm-bingo.js --warn --open # run the client app
```
