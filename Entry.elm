module Entry
    exposing
        ( Entry
        , getEntries
        , markEntryWithId
        , sumMarkedPoints
        , viewEntryList
        )

-- EXTERNAL MODULES

import Html
    exposing
        ( Html
        , li
        , span
        , text
        , ul
        )
import Html.Attributes exposing (class, classList)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (decode, required, optional, hardcoded)


-- APP MODULES

import App exposing (apiUrlPrefix)


type alias Entry =
    { id : Int
    , phrase : String
    , points : Int
    , marked : Bool
    }


allEntriesMarked : List Entry -> Bool
allEntriesMarked entries =
    List.all .marked entries


markEntryWithId : List Entry -> Int -> List Entry
markEntryWithId entries id =
    let
        markEntry e =
            if e.id == id then
                { e | marked = (not e.marked) }
            else
                e
    in
        List.map markEntry entries


entriesUrl : String
entriesUrl =
    apiUrlPrefix ++ "/random-entries"


entryDecoder : Decoder Entry
entryDecoder =
    decode Entry
        |> required "id" Decode.int
        |> required "phrase" Decode.string
        |> optional "points" Decode.int 0
        |> hardcoded False


getEntries : (Result Http.Error (List Entry) -> msg) -> Cmd msg
getEntries msg =
    entryListDecoder
        |> Http.get entriesUrl
        |> Http.send msg


entryListDecoder : Decoder (List Entry)
entryListDecoder =
    Decode.list entryDecoder


viewEntryItem : (Int -> msg) -> Entry -> Html msg
viewEntryItem msg entry =
    li [ classList [ ( "marked", entry.marked ) ], onClick (msg entry.id) ]
        [ span [ class "phrase" ] [ text entry.phrase ]
        , span [ class "points" ] [ text (toString entry.points) ]
        ]


viewEntryList : (Int -> msg) -> List Entry -> Html msg
viewEntryList msg entries =
    -- TODO: get this to work with |>
    let
        listOfEntries =
            List.map (viewEntryItem msg) entries
    in
        ul [] listOfEntries


sumMarkedPoints : List Entry -> Int
sumMarkedPoints entries =
    entries
        |> List.filter .marked
        |> List.foldl (\e sum -> sum + e.points) 0
