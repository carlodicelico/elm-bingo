module Score exposing (Score, postScore, viewScore)

import Html
    exposing
        ( Html
        , div
        , span
        , text
        )
import Html.Attributes exposing (class)
import Http
import Json.Decode as Decode exposing (Decoder, field, succeed)
import Json.Decode.Pipeline exposing (decode, required, optional, hardcoded)
import Json.Encode as Encode


-- APP MODULES

import App exposing (apiUrlPrefix)


type alias Score =
    { id : Int
    , name : String
    , score : Int
    }


postScore : (Result Http.Error Score -> msg) -> String -> Int -> Cmd msg
postScore msg name score =
    let
        url =
            App.apiUrlPrefix ++ "/scores"

        body =
            encodeScore name score
                |> Http.jsonBody

        request =
            Http.post url body scoreDecoder
    in
        Http.send msg request


viewScore : Int -> Html msg
viewScore sum =
    div
        [ class "score" ]
        [ span [ class "label" ] [ text "Score" ]
        , span [ class "value" ] [ text (toString sum) ]
        ]


encodeScore : String -> Int -> Encode.Value
encodeScore name score =
    Encode.object
        [ ( "name", Encode.string name )
        , ( "score", Encode.int score )
        ]


scoreDecoder : Decoder Score
scoreDecoder =
    decode Score
        |> required "id" Decode.int
        |> required "name" Decode.string
        |> required "score" Decode.int
