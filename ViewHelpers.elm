module ViewHelpers exposing (..)

import Html exposing (button, div, Html, span, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)


primaryButton : msg -> String -> Html msg
primaryButton clickHandler textAttr =
    button [ class "primary", onClick clickHandler ] [ text textAttr ]


viewAlertMessage : msg -> Maybe String -> Html msg
viewAlertMessage msg alertMessage =
    case alertMessage of
        Just message ->
            div [ class "alert" ]
                [ span [ class "close", onClick msg ] [ text "X" ]
                , text message
                ]

        Nothing ->
            text ""
