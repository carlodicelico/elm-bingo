module Bingo exposing (..)

-- EXTERNAL MODULES

import Html
    exposing
        ( a
        , button
        , div
        , footer
        , h1
        , h2
        , Html
        , header
        , input
        , li
        , span
        , text
        , ul
        )
import Html.Attributes
    exposing
        ( autofocus
        , class
        , classList
        , disabled
        , href
        , id
        , placeholder
        , rel
        , target
        , type_
        , value
        )
import Html.Events exposing (onClick, onInput)
import Http
import Random


-- APP MODULES

import Entry exposing (Entry, getEntries, markEntryWithId, sumMarkedPoints, viewEntryList)
import Score exposing (Score, postScore, viewScore)
import ViewHelpers exposing (primaryButton, viewAlertMessage)


-- MODEL


type GameState
    = EnteringName
    | Playing


type alias Model =
    { name : String
    , gameNumber : Int
    , entries : List Entry.Entry
    , alertMessage : Maybe String
    , nameInput : String
    , gameState : GameState
    }


initialModel : Model
initialModel =
    { name = "Anon E. Mouse"
    , gameNumber = 1
    , entries = []
    , alertMessage = Nothing
    , nameInput = ""
    , gameState = EnteringName
    }



-- UPDATE


type Msg
    = NewGame
    | Mark Int
    | Sort
    | NewRandom Int
    | NewEntries (Result Http.Error (List Entry.Entry))
    | CloseAlert
    | ShareScore
    | NewScore (Result Http.Error Score)
    | SetNameInput String
    | SaveName
    | ClearName
    | ChangeGameState GameState


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeGameState state ->
            ( { model | gameState = state }, Cmd.none )

        SetNameInput name ->
            ( { model | nameInput = name }, Cmd.none )

        SaveName ->
            if String.isEmpty model.nameInput then
                ( model, Cmd.none )
            else
                ( { model
                    | gameState = Playing
                    , name = model.nameInput
                    , nameInput = ""
                  }
                , Cmd.none
                )

        ClearName ->
            ( { model | gameState = Playing, nameInput = "" }, Cmd.none )

        NewRandom randomNumber ->
            ( { model | gameNumber = randomNumber }, Cmd.none )

        ShareScore ->
            ( model, Score.postScore NewScore model.name (Entry.sumMarkedPoints model.entries) )

        NewScore (Ok score) ->
            let
                message =
                    "Your score of "
                        ++ (toString score.score)
                        ++ " was successfully shared!"
            in
                ( { model | alertMessage = Just message }, Cmd.none )

        NewScore (Err error) ->
            ( { model | alertMessage = Just (httpErrorToMessage error) }, Cmd.none )

        NewGame ->
            -- Cmd.batch shorthand
            model ! [ generateRandomNumber, (Entry.getEntries NewEntries) ]

        NewEntries result ->
            case result of
                Ok randomEntries ->
                    ( { model | entries = List.sortBy .points randomEntries }, Cmd.none )

                Err error ->
                    ( { model | alertMessage = Just (httpErrorToMessage error) }, Cmd.none )

        CloseAlert ->
            ( { model | alertMessage = Nothing }, Cmd.none )

        Mark id ->
            ( { model | entries = Entry.markEntryWithId model.entries id }, Cmd.none )

        Sort ->
            ( { model | entries = List.sortBy .points model.entries }, Cmd.none )


httpErrorToMessage : Http.Error -> String
httpErrorToMessage error =
    case error of
        Http.NetworkError ->
            "Is the server running?"

        Http.BadStatus response ->
            -- I could also match on specific HTTP codes here...
            (toString response.status)

        Http.BadPayload message _ ->
            "Decoding failed: " ++ message

        _ ->
            (toString error)



-- COMMANDS


generateRandomNumber : Cmd Msg
generateRandomNumber =
    Random.generate NewRandom (Random.int 1 100)



-- VIEW


viewHeader : String -> Html Msg
viewHeader title =
    header []
        [ h1 [] [ text title ] ]


viewPlayer : String -> Int -> Html Msg
viewPlayer name gameNumber =
    h2 [ id "info", class "classy" ]
        [ a [ href "#", onClick (ChangeGameState EnteringName) ]
            [ text name ]
        , text (" - Game #" ++ (toString gameNumber))
        ]


viewFooter : Html Msg
viewFooter =
    footer []
        [ a
            [ href "https://github.com/carlodicelico/elm-bingo"
            , rel "noopener noreferrer"
            , target "_blank"
            ]
            [ text "Elm Buzzword Bingo on Github" ]
        ]


view : Model -> Html Msg
view model =
    div [ class "content" ]
        [ viewHeader "Elm Buzzword Bingo!"
        , viewPlayer model.name model.gameNumber
        , viewAlertMessage CloseAlert model.alertMessage
        , viewNameInput model
        , Entry.viewEntryList Mark model.entries
        , Score.viewScore (Entry.sumMarkedPoints model.entries)
        , div [ class "button-group" ]
            [ primaryButton NewGame "New Game"
              --, button [ class "primary", onClick ShareScore, disabled (isScoreZero model) ] [ text "Share Score" ]
            , primaryButton ShareScore "Share Score"
            , primaryButton Sort "Sort"
            ]
        , viewFooter
        ]


viewNameInput : Model -> Html Msg
viewNameInput model =
    case model.gameState of
        EnteringName ->
            div [ class "name-input" ]
                [ input
                    [ type_ "text"
                    , placeholder "Who's playing?"
                    , autofocus True
                    , value model.nameInput
                    , onInput SetNameInput
                    ]
                    []
                , primaryButton SaveName "Save"
                , primaryButton ClearName "Cancel"
                ]

        Playing ->
            text ""


init : ( Model, Cmd Msg )
init =
    ( initialModel, (Entry.getEntries NewEntries) )


main : Program Never Model Msg
main =
    Html.program
        { init =
            -- Cmd.batch shorthand
            initialModel ! [ generateRandomNumber, (Entry.getEntries NewEntries) ]
        , view = view
        , update = update
        , subscriptions = (always Sub.none)
        }
